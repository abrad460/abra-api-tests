# ABRA API tests

## SmartMeter

### Instalace
Pro testování použijeme nejnovější verzi 1.0.5  

Ke stažení na https://www.smartmeter.io/download  

Na stejné stránce (pod odkazy na stažení) je nutné ještě vyplnit formulář (jméno a email) pro získání free licence.  

Stažený soubor (SmartMeter_1.0.5x_Light.zip) pak rozbalte a do podadresáře "**/custom**" nahrajte licenční klíč "**license.bin**", který by měl obratem přijít na zadaný email.   

### Spuštění testu
- **SmartMeter.exe** (**.sh** pro Linux nebo **.command** pro Mac)
- kliknout na **Create/Edit Scenario**
- otevřít test (soubor s příponou **.jmx**) File -> Open ...
- kliknout na **Run SmartMeter Test** (červená šipka na konci toolbaru)
- otevře se okno s průběhem testu
- po skončení testu se výsledný report vygeneruje kliknutím na tlačítko v toolbaru se symbolem grafu (jsou tam jen dvě tlačítka)
- vysvětlení ke grafům v reportu je na https://www.smartmeter.io/documentation#toc-test-report-graphs-overview
